import axios from "axios";

const instance = axios.create({
    baseURL: "http://localhost:8000"
});
const http = instance

function PredicEmail(data: string) {
    const formDate = new FormData();
    formDate.append("item", data);
    return http.put("/Email", formDate,{headers: {
        "Content-Type" : "multipart/form-data"
      }});
}

export default { PredicEmail }